// stdlib functionality
#include <iostream>
#include <fstream>
// ROOT functionality
#include <TFile.h>
#include <TH1D.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"

TString myParser(std::string configFilePath, std::string config_string)
{
	std::ifstream myStream;
	myStream.open(configFilePath);
	TString temp_string = "";
	std::string line;
	while (getline(myStream, line))
	{
		if (line.find(config_string,0) != std::string::npos)
		{
			temp_string = line.substr(line.find("=")+1);
		}
	}
	myStream.close();
	return temp_string;
}

int main(int argc, char* argv[]) {

	if (argc != 2)
	{
		std::cerr << "Requires one argument (path to the configuration file)" << std::endl;
		return 1;
	}

	// open the configuration file
	std::string configFilePath = argv[1];
	std::cout << configFilePath << std::endl;

  // initialize the xAOD EDM
  xAOD::Init();

  // open the input file
  TString inputFilePath = myParser(configFilePath, "inputFilePath");
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  event.readFrom( iFile.get() );

  // make histograms and output file
  TH1D* hist_jets_n = new TH1D("hist_jets_n","Number of Jets",20,0,20);
  TH1D* hist_dijet_M = new TH1D("hist_dijet_M","Dijet Invariant Mass;Dijet Invariant Mass [GeV];",100,0,500);

  TString outputFilePath = "/home/atlas/Bootcamp/Output/output_hists.root";
  TFile* outfile = TFile::Open(outputFilePath,"RECREATE");

  // enable selection
  bool eventPass = false;
  bool cut_jet_pt_min = (myParser(configFilePath, "jet_pt_min") != "") ? true : false;
  bool cut_jet_eta_abs_max = (myParser(configFilePath, "jet_eta_abs_max") != "") ? true : false;

  // for counting events
  unsigned count = 0;

  // get the number of events in the file to loop over
  const Long64_t numEntries = event.getEntries();

  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;

    eventPass = true;

    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMTopoJets");

    if (jets->size() >= 2)
    {

      // loop through all of the jets and make selections with the helper
      for(const xAOD::Jet* jet : *jets) {
        // print the kinematics of each jet in the event
        std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << "  phi=" << jet->phi() << "  m=" << jet->m() << std::endl;
      }

      // the jets are already sorted by pt so the two leading jets are at 0 and 1
      if (cut_jet_pt_min)
      {
      	Double_t jet_pt_min = myParser(configFilePath, "jet_pt_min").Atof();
      	if (!(jets->at(0)->pt()/1000. > jet_pt_min && jets->at(1)->pt()/1000. > jet_pt_min))
      	{
      		eventPass = false;
      	}
      }
      if (cut_jet_eta_abs_max)
      {
      	Double_t jet_eta_abs_max = myParser(configFilePath, "jet_eta_abs_max").Atof();
      	if (!(std::abs(jets->at(0)->eta()) < jet_eta_abs_max && std::abs(jets->at(1)->eta()) < jet_eta_abs_max))
      	{
      		eventPass = false;
      	}
      }

    }
    else
    {
    	eventPass = false;
    }

    if (eventPass)
    {
    	hist_jets_n->Fill(jets->size());
      hist_dijet_M->Fill( (jets->at(0)->p4() + jets->at(1)->p4()).M()/1000. );
    }

    // counter for the number of events analyzed thus far
    count += 1;
  }

  // write hist and close file
  outfile->cd();
  hist_jets_n->Write();
  hist_dijet_M->Write();
  outfile->Close();

  delete hist_jets_n;
  delete hist_dijet_M;

  // exit from the main function cleanly
  return 0;
}